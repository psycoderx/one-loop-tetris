# One Loop Tetris

OLT is a project I'm developing just for fun.
The goal is to create a clone of a world famous game using no more than one loop.

## Da Roolez

1. There must be only one loop statement in the source code.
2. Recursion is not allowed due its loopability.
3. Third-party dependances are allowed but only for non-game-logic part such as graphics and sound.

