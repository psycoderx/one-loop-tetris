////////////////////////////////////////////////////////////////
// One Loop Tetris
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// INCLUDES
////////////////////////////////////////////////////////////////

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <GL/glew.h>
#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

////////////////////////////////////////////////////////////////
// MACROS
////////////////////////////////////////////////////////////////

#define errorf(...) fprintf(stderr, __VA_ARGS__)

#define size_from_tex_size(tsize) (((GLfloat)(tsize) / (TEX_BLOCK_W)) * BLOCK_W)

////////////////////////////////////////////////////////////////
// DEFINES
////////////////////////////////////////////////////////////////

#define TEX_W           128
#define TEX_H           128
#define TEX_BLOCK_X0    1
#define TEX_BLOCK_Y0    1
#define TEX_BLOCK_W     16
#define TEX_BLOCK_H     16
#define TEX_BLOCK_DX    (TEX_BLOCK_W + 1)
#define TEX_BLOCK_DY    (TEX_BLOCK_H + 1)
#define TEX_BRICKS_X    69
#define TEX_BRICKS_Y    1
#define TEX_BRICKS_W    32
#define TEX_BRICKS_H    32
#define TEX_WALL_X      102
#define TEX_WALL_Y      1
#define TEX_WALL_W      16
#define TEX_WALL_H      32
#define TEX_FLOOR_X     69
#define TEX_FLOOR_Y     34
#define TEX_FLOOR_W     32
#define TEX_FLOOR_H     16
#define TEX_CORNER_X    102
#define TEX_CORNER_Y    34
#define TEX_CORNER_W    16
#define TEX_CORNER_H    16
#define TEX_DIGIT_X0    1
#define TEX_DIGIT_Y0    69
#define TEX_DIGIT_W     7
#define TEX_DIGIT_H     7
#define TEX_DIGIT_DX    (TEX_DIGIT_W + 1)
#define TEX_SCORE_X     1
#define TEX_SCORE_Y     77
#define TEX_SCORE_W     31
#define TEX_SCORE_H     7
#define TEX_GAME_OVER_X 33
#define TEX_GAME_OVER_Y 77
#define TEX_GAME_OVER_W 55
#define TEX_GAME_OVER_H 7
#define TEX_LOGO_X      1
#define TEX_LOGO_Y      85
#define TEX_LOGO_W      30
#define TEX_LOGO_H      11

#define BLOCK_W     (2.0 / 24)
#define BLOCK_H     BLOCK_W
#define BRICKS_W    size_from_tex_size(TEX_BRICKS_W)
#define BRICKS_H    size_from_tex_size(TEX_BRICKS_H)
#define WALL_W      size_from_tex_size(TEX_WALL_W)
#define WALL_H      size_from_tex_size(TEX_WALL_H)
#define FLOOR_W     size_from_tex_size(TEX_FLOOR_W)
#define FLOOR_H     size_from_tex_size(TEX_FLOOR_H)
#define CORNER_W    size_from_tex_size(TEX_CORNER_W)
#define CORNER_H    size_from_tex_size(TEX_CORNER_H)
#define DIGIT_W     size_from_tex_size(TEX_DIGIT_W)
#define DIGIT_H     size_from_tex_size(TEX_DIGIT_H)
#define SCORE_W     size_from_tex_size(TEX_SCORE_W)
#define SCORE_H     size_from_tex_size(TEX_SCORE_H)
#define GAME_OVER_W size_from_tex_size(TEX_GAME_OVER_W)
#define GAME_OVER_H size_from_tex_size(TEX_GAME_OVER_H)
#define LOGO_W      size_from_tex_size(TEX_LOGO_W)
#define LOGO_H      size_from_tex_size(TEX_LOGO_H)

////////////////////////////////////////////////////////////////
// TYPEDEFS
////////////////////////////////////////////////////////////////

enum {
  ET_OK = 1,
  ET_FAIL,
};

enum {
  BT_C,
  BT_I,
  BT_L,
  BT_T,
};

enum {
  BR_L,
  BR_T,
  BR_R,
  BR_B,
};

enum {
  P_IH,
  P_IV,
  P_J0,
  P_J1,
  P_J2,
  P_J3,
  P_L0,
  P_L1,
  P_L2,
  P_L3,
  P_O,
  P_SH,
  P_SV,
  P_T0,
  P_T1,
  P_T2,
  P_T3,
  P_ZH,
  P_ZV,
  P_COUNT,
};

enum {
  TR_AS_IS,
  TR_FLIP_X,
};

enum {
  ST_NEXT_PIECE,
  ST_DRAW_BEGIN,
  ST_DRAW_BG,
  ST_DRAW_WORLD,
  ST_DRAW_PIECE,
  ST_DRAW_NEXT_PIECE,
  ST_DRAW_END,
  ST_UPD_BEGIN,
  ST_MOVE_DOWN,
  ST_MOVE_DOWN_HIT,
  ST_MOVE_V,
  ST_MOVE_V_BACK,
  ST_ROTATE,
  ST_ROTATE_BACK,
  ST_REMOVE_LINES_INIT,
  ST_REMOVE_LINES,
  ST_SHIFT_LINES,
  ST_PLACE,
  ST_HIT_TEST,
  ST_UPD_END,
};

typedef struct Olt_Vec3gf {
  GLfloat x; // r
  GLfloat y; // g
  GLfloat z; // b
} Olt_Vec3gf;

typedef struct Olt_Block {
  int type;     // 0 .. 3; > = L T
  int rotation; // 0 .. 3; > v < ^
  int color;    // 0 .. 7;
  bool is_not_null;
  bool is_outside;
} Olt_Block;

typedef struct Olt_Piece {
  Olt_Block blocks[4 * 4];
  int rotated;
  int rotated_back;
} Olt_Piece;

typedef struct Olt_Global_Vars {
  Olt_Piece pieces[P_COUNT];
  Olt_Block world[10 * 20];
  GLFWwindow *window;
  //
  // time
  //
  double dt;
  double prev_time;
  double step_time;
  double step_time_acc;
  //
  // CloseGL
  //
  GLuint program;
  GLuint shader;
  GLuint texture;
  GLint uniform_texture;
  //
  // current piece
  //
  int piece;
  int piece_x;
  int piece_y;
  int piece_color;
  int next_piece;
  int next_piece_color;
  //
  // ...
  //
  int score;
  //
  // loop registers
  //
  int state;
  int state_after;
  int state_if_hit;
  int ux;
  int uy;
  int uw;
  int uh;
  int ui;
  int un;
  int removed_lines[4];
  int piece_dx;
  bool contains_air;
  bool game_over;
  //
  // glfw state
  //
  bool glfw_inited;
  //
  // key states
  //
  bool pressed_rotate;
  bool pressed_left;
  bool pressed_right;
  bool pressed_place;
  bool clicked_rotate;
  bool clicked_left;
  bool clicked_right;
  bool clicked_place;
} Olt_Global_Vars;

////////////////////////////////////////////////////////////////
// FUNCTIONS
////////////////////////////////////////////////////////////////

void olt_cb_key(GLFWwindow* window, int key, int scancode, int action, int mods);

void olt_cb_window_size(GLFWwindow* window, int width, int height);

void olt_trect(GLfloat x, GLfloat y, GLfloat w, GLfloat h, int tx, int ty, int tw, int th, int flags);

void olt_draw_block(const Olt_Block *block, int wx, int wy, int color);

Olt_Vec3gf olt_unpack_color(int color);

void olt_register_key(int rkey, int key, int action, bool *pressed, bool *clicked);

GLuint olt_texture_from_file(const char *filename);

GLuint olt_shader_from_string(GLenum type, const char *src, const char *srcname);

Olt_Block olt_piece_block(int type, int rotation);

Olt_Block *olt_block_at(int x, int y, Olt_Block *a, int w, int h);

void olt_init(void);

void olt_loop(void);

void olt_exit(int exit_type);

////////////////////////////////////////////////////////////////
// VARIABLES
////////////////////////////////////////////////////////////////

static Olt_Global_Vars static_olt;
Olt_Global_Vars *olt = &static_olt;

////////////////////////////////////////////////////////////////
// IMPLEMENTATION
////////////////////////////////////////////////////////////////

void olt_loop(void)
{
  /**/ if (olt->state == ST_DRAW_BEGIN) {
    if (glfwWindowShouldClose(olt->window)) {
      olt_exit(ET_OK);
    }
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(olt->program);
    glUniform1i(olt->uniform_texture, 0);
    glBindTexture(GL_TEXTURE_2D, olt->texture);
    olt->state = ST_DRAW_BG;
    olt->ux = 0;
    olt->uy = 0;
    olt->uw = 5;
    olt->uh = 10;
  }
  else if (olt->state == ST_DRAW_BG) {
    if (olt->uy >= olt->uh) {
      GLfloat x0 = -1.0;
      GLfloat x1 = -1.0 + WALL_W + 10 * BLOCK_W;
      GLfloat y =  1.0 - CORNER_H - 10 * WALL_H;
      glColor3f(1.0, 0.0, 0.0);
      olt_trect(x0, y, CORNER_W, CORNER_H, TEX_CORNER_X, TEX_CORNER_Y, TEX_CORNER_W, TEX_CORNER_H, TR_FLIP_X);
      olt_trect(x1, y, CORNER_W, CORNER_H, TEX_CORNER_X, TEX_CORNER_Y, TEX_CORNER_W, TEX_CORNER_H, TR_AS_IS);
      GLfloat score_x = 0.1;
      GLfloat score_y = 0.8 + DIGIT_H * 1.5;
      glColor3f(1.0, 1.0, 1.0);
      olt_trect(score_x, score_y, SCORE_W, SCORE_H, TEX_SCORE_X, TEX_SCORE_Y, TEX_SCORE_W, TEX_SCORE_H, TR_AS_IS);
      GLfloat logo_k = 3.0;
      GLfloat logo_x = -1.0 + WALL_W + 5 * BLOCK_W - (LOGO_W * logo_k) / 2;
      GLfloat logo_y = -0.9;
      glColor3f(1.0, 0.0, 0.0);
      olt_trect(logo_x, logo_y, LOGO_W * logo_k, LOGO_H * logo_k, TEX_LOGO_X, TEX_LOGO_Y, TEX_LOGO_W, TEX_LOGO_H, TR_AS_IS);
      olt->ux = 0;
      olt->uy = 0;
      olt->uw = 10;
      olt->uh = 20;
      olt->state = ST_DRAW_WORLD;
      return;
    }
    if (olt->ux >= olt->uw) {
      GLfloat x0 = -1.0;
      GLfloat x1 = -1.0 + WALL_W + 10 * BLOCK_W;
      GLfloat y =  1.0 - WALL_H - olt->uy * WALL_H;
      glColor3f(1.0, 0.0, 0.0);
      olt_trect(x0, y, WALL_W, WALL_H, TEX_WALL_X, TEX_WALL_Y, TEX_WALL_W, TEX_WALL_H, TR_FLIP_X);
      olt_trect(x1, y, WALL_W, WALL_H, TEX_WALL_X, TEX_WALL_Y, TEX_WALL_W, TEX_WALL_H, TR_AS_IS);
      //
      int power = pow(10, 9 - olt->uy);
      int digit = (olt->score / power) % 10;
      GLfloat digit_x = 0.1 + olt->uy * DIGIT_W;
      GLfloat digit_y = 0.8;
      int digit_tx = TEX_DIGIT_X0 + TEX_DIGIT_DX * digit;
      glColor3f(1.0, 1.0, 1.0);
      olt_trect(digit_x, digit_y, DIGIT_W, DIGIT_H, digit_tx, TEX_DIGIT_Y0, TEX_DIGIT_W, TEX_DIGIT_H, TR_AS_IS);
      //
      olt->uy += 1;
      olt->ux = 0;
      return;
    }
    GLfloat x = -1.0 + WALL_W + olt->ux * BRICKS_W;
    GLfloat y =  1.0 - BRICKS_H - olt->uy * BRICKS_H;
    glColor3f(1.0, 0.0, 0.0);
    olt_trect(x, y, BRICKS_W, BRICKS_H, TEX_BRICKS_X, TEX_BRICKS_Y, TEX_BRICKS_W, TEX_BRICKS_H, TR_AS_IS);
    if (olt->uy == 0) {
      GLfloat x = -1.0 + CORNER_W + olt->ux * FLOOR_W;
      GLfloat y =  1.0 - CORNER_H - 10 * WALL_H;
      glColor3f(1.0, 0.0, 0.0);
      olt_trect(x, y, FLOOR_W, FLOOR_H, TEX_FLOOR_X, TEX_FLOOR_Y, TEX_FLOOR_W, TEX_FLOOR_H, TR_AS_IS);
    }
    olt->ux += 1;
  }
  else if (olt->state == ST_DRAW_WORLD) {
    if (olt->uy >= olt->uh) {
      olt->state = ST_DRAW_PIECE;
      olt->ux = 0;
      olt->uy = 0;
      olt->uw = 4;
      olt->uh = 4;
      return;
    }
    if (olt->ux >= olt->uw) {
      olt->uy += 1;
      olt->ux = 0;
      return;
    }
    Olt_Block *block = olt_block_at(olt->ux, olt->uy, olt->world, 10, 20);
    olt_draw_block(block, olt->ux, olt->uy, block->color);
    olt->ux += 1;
  }
  else if (olt->state == ST_DRAW_PIECE) {
    if (olt->uy >= olt->uh) {
      olt->ux = 0;
      olt->uy = 0;
      olt->uw = 4;
      olt->uh = 4;
      olt->state = ST_DRAW_NEXT_PIECE;
      return;
    }
    if (olt->ux >= olt->uw) {
      olt->uy += 1;
      olt->ux = 0;
      return;
    }
    Olt_Piece *piece = &olt->pieces[olt->piece];
    Olt_Block *block = olt_block_at(olt->ux, olt->uy, piece->blocks, 4, 4);
    int wx = olt->piece_x + olt->ux;
    int wy = olt->piece_y + olt->uy;
    olt_draw_block(block, wx, wy, olt->piece_color);
    olt->ux += 1;
  }
  else if (olt->state == ST_DRAW_NEXT_PIECE) {
    if (olt->uy >= olt->uh) {
      olt->state = ST_DRAW_END;
      return;
    }
    if (olt->ux >= olt->uw) {
      olt->uy += 1;
      olt->ux = 0;
      return;
    }
    Olt_Piece *piece = &olt->pieces[olt->next_piece];
    Olt_Block *block = olt_block_at(olt->ux, olt->uy, piece->blocks, 4, 4);
    int wx = 14 + olt->ux;
    int wy = 6 + olt->uy;
    olt_draw_block(block, wx, wy, olt->next_piece_color);
    olt->ux += 1;
  }
  else if (olt->state == ST_DRAW_END) {
    if (olt->game_over) {
      GLfloat gv_k = 2.5;
      GLfloat gv_w = GAME_OVER_W * gv_k;
      GLfloat gv_h = GAME_OVER_H * gv_k;
      GLfloat gv_x = -1.0 + WALL_W + 5 * BLOCK_W - gv_w / 2.0;
      GLfloat gv_y = 1.0 - 10 * BLOCK_H - gv_h / 2.0;
      glColor3f(1.0, 0.0, 0.0);
      olt_trect(gv_x, gv_y, gv_w, gv_h, TEX_GAME_OVER_X, TEX_GAME_OVER_Y, TEX_GAME_OVER_W, TEX_GAME_OVER_H, TR_AS_IS);
    }
    //
    glfwSwapBuffers(olt->window);
    //
    // - Why not to use loop in here?
    // - YES! But unfortunately it's a challenge where I cannot use loops.
    //
    olt->clicked_left = false;
    olt->clicked_right = false;
    olt->clicked_rotate = false;
    olt->clicked_place = false;
    glfwPollEvents();
    if (!olt->game_over) {
      olt->state = ST_UPD_BEGIN;
    }
    else {
      olt->state = ST_DRAW_BEGIN;
    }
  }
  else if (olt->state == ST_UPD_BEGIN) {
    double curr_time = glfwGetTime();
    olt->dt = curr_time - olt->prev_time;
    olt->prev_time = curr_time;
    olt->step_time_acc += olt->dt;
    olt->ui = 0;
    olt->un = 0;
    if (olt->step_time_acc > olt->step_time) {
      olt->step_time_acc -= olt->step_time;
      olt->un = 1;
    }
    if (olt->clicked_place) {
      olt->un = 25;
    }
    olt->state = ST_MOVE_DOWN;
  }
  else if (olt->state == ST_MOVE_DOWN) {
    if (olt->ui >= olt->un) {
      olt->piece_dx = 0;
      if (olt->clicked_left) {
        olt->piece_dx = -1;
      }
      else if (olt->clicked_right) {
        olt->piece_dx = 1;
      }
      olt->state = ST_MOVE_V;
      return;
    }
    olt->piece_y += 1;
    olt->ux = 0;
    olt->uy = 0;
    olt->uw = 4;
    olt->uh = 4;
    olt->state = ST_HIT_TEST;
    olt->state_after = ST_MOVE_DOWN;
    olt->state_if_hit = ST_MOVE_DOWN_HIT;
    olt->ui++;
  }
  else if (olt->state == ST_MOVE_DOWN_HIT) {
    olt->piece_y -= 1;
    olt->ux = 0;
    olt->uy = 0;
    olt->uw = 4;
    olt->uh = 4;
    olt->state = ST_PLACE;
  }
  else if (olt->state == ST_MOVE_V) {
    olt->piece_x += olt->piece_dx;
    olt->ux = 0;
    olt->uy = 0;
    olt->uw = 4;
    olt->uh = 4;
    olt->state = ST_HIT_TEST;
    olt->state_after = ST_ROTATE;
    olt->state_if_hit = ST_MOVE_V_BACK;
  }
  else if (olt->state == ST_MOVE_V_BACK) {
    olt->piece_x -= olt->piece_dx;
    olt->state = ST_ROTATE;
  }
  else if (olt->state == ST_ROTATE) {
    if (!olt->clicked_rotate) {
      olt->state = ST_REMOVE_LINES_INIT;
      return;
    }
    olt->piece = olt->pieces[olt->piece].rotated;
    olt->ux = 0;
    olt->uy = 0;
    olt->uw = 4;
    olt->uh = 4;
    olt->state = ST_HIT_TEST;
    olt->state_after = ST_REMOVE_LINES_INIT;
    olt->state_if_hit = ST_ROTATE_BACK;
  }
  else if (olt->state == ST_ROTATE_BACK) {
    olt->piece = olt->pieces[olt->piece].rotated_back;
    olt->state = ST_REMOVE_LINES_INIT;
  }
  else if (olt->state == ST_PLACE) {
    if (olt->uy >= olt->uh) {
      olt->state = ST_NEXT_PIECE;
      return;
    }
    if (olt->ux >= olt->uw) {
      olt->uy += 1;
      olt->ux = 0;
      return;
    }
    Olt_Piece *piece = &olt->pieces[olt->piece];
    Olt_Block *pblock = olt_block_at(olt->ux, olt->uy, piece->blocks, 4, 4);
    int wx = olt->piece_x + olt->ux;
    int wy = olt->piece_y + olt->uy;
    Olt_Block *wblock = olt_block_at(wx, wy, olt->world, 10, 20);
    if (pblock->is_not_null) {
      if (wy < 0) {
        olt->game_over = true;
      }
      *wblock = *pblock;
      wblock->color = olt->piece_color;
    }
    olt->ux += 1;
  }
  else if (olt->state == ST_REMOVE_LINES_INIT) {
    olt->ui = 0;
    olt->ux = 0;
    olt->uy = 0;
    olt->uw = 10;
    olt->uh = 20;
    olt->contains_air = false;
    olt->state = ST_REMOVE_LINES;
  }
  else if (olt->state == ST_REMOVE_LINES) {
    int wx = olt->ux;
    int wy = 19 - olt->uy;
    if (olt->uy >= olt->uh) {
      int bonus[5] = { 0, 100, 250, 550, 1000 };
      olt->score += bonus[olt->ui];
      olt->ui -= 1;
      olt->ux = 0;
      olt->uy = 0;
      olt->uw = 10;
      olt->uh = 20;
      olt->state = ST_SHIFT_LINES;
      return;
    }
    if (olt->ux >= olt->uw) {
      if (!olt->contains_air) {
        //
        // It isn't a loop!
        //
        memset(&olt->world[wy * 10], 0, sizeof(olt->world[0]) * 10);
        if (olt->ui >= 4) {
          errorf("%s", "Unreachable error with number of lines removed more than 4\n");
          olt_exit(ET_FAIL);
        }
        olt->removed_lines[olt->ui] = wy;
        olt->ui += 1;
      }
      olt->contains_air = false;
      olt->uy += 1;
      olt->ux = 0;
      return;
    }
    Olt_Block *block = olt_block_at(wx, wy, olt->world, 10, 20);
    if (!block->is_not_null) {
      olt->contains_air = true;
    }
    olt->ux += 1;
  }
  else if (olt->state == ST_SHIFT_LINES) {
    if (olt->ui < 0) {
      olt->ui = 0;
      olt->state = ST_UPD_END;
      return;
    }
    int wx = olt->ux;
    int wy = 19 - olt->uy;
    int ry = olt->removed_lines[olt->ui];
    if (olt->uy >= olt->uh) {
      olt->ui -= 1;
      olt->uy = 0;
      return;
    }
    if (olt->ux >= olt->uw) {
      olt->uy += 1;
      olt->ux = 0;
      return;
    }
    if (wy < ry) {
      Olt_Block *wblock0 = olt_block_at(wx, wy, olt->world, 10, 20);
      Olt_Block *wblock1 = olt_block_at(wx, wy + 1, olt->world, 10, 20);
      *wblock1 = *wblock0;
    }
    olt->ux += 1;
  }
  else if (olt->state == ST_NEXT_PIECE) {
    olt->piece_x = 3;
    olt->piece_y = -4;
    olt->piece = olt->next_piece;
    olt->piece_color = olt->next_piece_color;
    olt->next_piece = rand() % P_COUNT;
    olt->next_piece_color = rand() % 7;
    olt->state = ST_DRAW_BEGIN;
  }
  else if (olt->state == ST_HIT_TEST) {
    if (olt->uy >= olt->uh) {
      olt->state = olt->state_after;
      return;
    }
    if (olt->ux >= olt->uw) {
      olt->uy += 1;
      olt->ux = 0;
      return;
    }
    Olt_Piece *piece = &olt->pieces[olt->piece];
    Olt_Block *pblock = olt_block_at(olt->ux, olt->uy, piece->blocks, 4, 4);
    int wx = olt->piece_x + olt->ux;
    int wy = olt->piece_y + olt->uy;
    Olt_Block *wblock = olt_block_at(wx, wy, olt->world, 10, 20);
    if (pblock->is_not_null && wblock->is_not_null) {
      olt->state = olt->state_if_hit;
      return;
    }
    olt->ux += 1;
  }
  else if (olt->state == ST_UPD_END) {
    olt->state = ST_DRAW_BEGIN;
  }
  else {
    errorf("%s%i%s", "Unknown state: ", olt->state, "\n");
    olt_exit(ET_FAIL);
  }
}

void olt_init(void)
{
  //
  // GLFW
  //
  if (!glfwInit()) {
    errorf("%s", "glfwInit() failed\n");
    olt_exit(ET_FAIL);
  }
  olt->glfw_inited = true;
  //
  // GLFW WINDOW
  //
  olt->window = glfwCreateWindow(400, 400, "One Loop Tetris", NULL, NULL);
  if (olt->window == NULL) {
    errorf("%s", "glfwCreateWindow() failed\n");
    olt_exit(ET_FAIL);
  }
  glfwSetKeyCallback(olt->window, olt_cb_key);
  glfwSetWindowSizeCallback(olt->window, olt_cb_window_size);
  glfwMaximizeWindow(olt->window);
  //
  // GLEW
  //
  glfwMakeContextCurrent(olt->window);
  if (glewInit() != GLEW_OK) {
    errorf("%s", "glewInit() failed\n");
    olt_exit(ET_FAIL);
  }
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);
  //
  // SHADER
  //
  #if 0
  // const lightness variant (unused)
  const char *shader_source =
    "#version 120                                     \n"
    "uniform sampler2D tex;                           \n"
    "void main()                                      \n"
    "{                                                \n"
    "  vec2 pos = gl_TexCoord[0].st;                  \n"
    "  vec4 p = texture2D(tex, pos);                  \n"
    "  vec3 c = gl_Color.rgb;                         \n"
    "  float k = c.r + c.g + c.b;                     \n"
    "  float a = p.a * gl_Color.a;                    \n"
    "  vec3 b2c = c * p.r / k;                        \n"
    "  vec3 c2w = b2c * (1 - p.g) + vec3(1) * (p.g);  \n"
    "  gl_FragColor = vec4(c2w, a);                   \n"
    "}                                                \n"
  ;
  #else
  const char *shader_source =
    "#version 120                                     \n"
    "uniform sampler2D tex;                           \n"
    "void main()                                      \n"
    "{                                                \n"
    "  vec2 pos = gl_TexCoord[0].st;                  \n"
    "  vec4 p = texture2D(tex, pos);                  \n"
    "  vec3 c = gl_Color.rgb;                         \n"
    "  float a = p.a * gl_Color.a;                    \n"
    "  vec3 b2c = c * p.r;                            \n"
    "  vec3 c2w = b2c * (1 - p.g) + vec3(1) * (p.g);  \n"
    "  gl_FragColor = vec4(c2w, a);                   \n"
    "}                                                \n"
  ;
  #endif
  olt->program = glCreateProgram();
  olt->shader = olt_shader_from_string(GL_FRAGMENT_SHADER, shader_source, "main_shader");
  glAttachShader(olt->program, olt->shader);
  glLinkProgram(olt->program);
  const char *uniform_texture_name = "tex";
  olt->uniform_texture = glGetUniformLocation(olt->program, uniform_texture_name);
  if (olt->uniform_texture < 0) {
    errorf("%s%s%s", "Uniform \"", uniform_texture_name, "\" is not found\n");
    olt_exit(ET_FAIL);
  }
  //
  // TEXTURE
  //
  olt->texture = olt_texture_from_file("./tex.128x128");
  //
  // TIME
  //
  olt->prev_time = glfwGetTime();
  olt->step_time = 0.2;
  //
  // PIECES
  //
  Olt_Piece *p = NULL;
  //
  p = &olt->pieces[P_IH];
  p->rotated = P_IV;
  p->rotated_back = P_IV;
  *olt_block_at(0, 2, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_R);
  *olt_block_at(1, 2, p->blocks, 4, 4) = olt_piece_block(BT_I, BR_R);
  *olt_block_at(2, 2, p->blocks, 4, 4) = olt_piece_block(BT_I, BR_R);
  *olt_block_at(3, 2, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_L);
  //
  p = &olt->pieces[P_IV];
  p->rotated = P_IH;
  p->rotated_back = P_IH;
  *olt_block_at(1, 0, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_B);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_I, BR_B);
  *olt_block_at(1, 2, p->blocks, 4, 4) = olt_piece_block(BT_I, BR_B);
  *olt_block_at(1, 3, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_T);
  //
  p = &olt->pieces[P_J0];
  p->rotated = P_J1;
  p->rotated_back = P_J3;
  *olt_block_at(1, 0, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_B);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_I, BR_T);
  *olt_block_at(1, 2, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_T);
  *olt_block_at(0, 2, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_R);
  //
  p = &olt->pieces[P_J1];
  p->rotated = P_J2;
  p->rotated_back = P_J0;
  *olt_block_at(0, 1, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_R);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_I, BR_L);
  *olt_block_at(2, 1, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_L);
  *olt_block_at(0, 0, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_B);
  //
  p = &olt->pieces[P_J2];
  p->rotated = P_J3;
  p->rotated_back = P_J1;
  *olt_block_at(1, 0, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_B);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_I, BR_T);
  *olt_block_at(1, 2, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_T);
  *olt_block_at(2, 0, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_L);
  //
  p = &olt->pieces[P_J3];
  p->rotated = P_J0;
  p->rotated_back = P_J2;
  *olt_block_at(0, 1, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_R);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_I, BR_L);
  *olt_block_at(2, 1, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_L);
  *olt_block_at(2, 2, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_T);
  //
  p = &olt->pieces[P_L0];
  p->rotated = P_L1;
  p->rotated_back = P_L3;
  *olt_block_at(1, 0, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_B);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_I, BR_B);
  *olt_block_at(1, 2, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_R);
  *olt_block_at(2, 2, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_L);
  //
  p = &olt->pieces[P_L1];
  p->rotated = P_L2;
  p->rotated_back = P_L0;
  *olt_block_at(0, 1, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_B);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_I, BR_L);
  *olt_block_at(2, 1, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_L);
  *olt_block_at(0, 2, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_T);
  //
  p = &olt->pieces[P_L2];
  p->rotated = P_L3;
  p->rotated_back = P_L1;
  *olt_block_at(1, 0, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_L);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_I, BR_T);
  *olt_block_at(1, 2, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_T);
  *olt_block_at(0, 0, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_R);
  //
  p = &olt->pieces[P_L3];
  p->rotated = P_L0;
  p->rotated_back = P_L2;
  *olt_block_at(0, 1, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_R);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_I, BR_L);
  *olt_block_at(2, 1, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_T);
  *olt_block_at(2, 0, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_B);
  //
  p = &olt->pieces[P_O];
  p->rotated = P_O;
  p->rotated_back = P_O;
  *olt_block_at(0, 0, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_B);
  *olt_block_at(0, 1, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_R);
  *olt_block_at(1, 0, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_L);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_T);
  //
  p = &olt->pieces[P_SH];
  p->rotated = P_SV;
  p->rotated_back = P_SV;
  *olt_block_at(0, 2, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_R);
  *olt_block_at(1, 2, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_T);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_B);
  *olt_block_at(2, 1, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_L);
  //
  p = &olt->pieces[P_SV];
  p->rotated = P_SH;
  p->rotated_back = P_SH;
  *olt_block_at(1, 0, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_B);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_R);
  *olt_block_at(2, 1, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_L);
  *olt_block_at(2, 2, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_T);
  //
  p = &olt->pieces[P_T0];
  p->rotated = P_T1;
  p->rotated_back = P_T3;
  *olt_block_at(1, 0, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_B);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_T, BR_R);
  *olt_block_at(1, 2, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_T);
  *olt_block_at(2, 1, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_L);
  //
  p = &olt->pieces[P_T1];
  p->rotated = P_T2;
  p->rotated_back = P_T0;
  *olt_block_at(0, 1, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_R);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_T, BR_B);
  *olt_block_at(2, 1, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_L);
  *olt_block_at(1, 2, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_T);
  //
  p = &olt->pieces[P_T2];
  p->rotated = P_T3;
  p->rotated_back = P_T1;
  *olt_block_at(1, 0, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_B);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_T, BR_L);
  *olt_block_at(1, 2, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_T);
  *olt_block_at(0, 1, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_R);
  //
  p = &olt->pieces[P_T3];
  p->rotated = P_T0;
  p->rotated_back = P_T2;
  *olt_block_at(0, 1, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_R);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_T, BR_T);
  *olt_block_at(2, 1, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_L);
  *olt_block_at(1, 0, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_B);
  //
  p = &olt->pieces[P_ZH];
  p->rotated = P_ZV;
  p->rotated_back = P_ZV;
  *olt_block_at(0, 1, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_R);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_L);
  *olt_block_at(1, 2, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_R);
  *olt_block_at(2, 2, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_L);
  //
  p = &olt->pieces[P_ZV];
  p->rotated = P_ZH;
  p->rotated_back = P_ZH;
  *olt_block_at(2, 0, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_B);
  *olt_block_at(2, 1, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_T);
  *olt_block_at(1, 1, p->blocks, 4, 4) = olt_piece_block(BT_L, BR_B);
  *olt_block_at(1, 2, p->blocks, 4, 4) = olt_piece_block(BT_C, BR_T);
  //
  //
  //
  srand(time(NULL));
  olt->next_piece = rand() % P_COUNT;
  olt->next_piece_color = rand() % 7;
  olt->state = ST_NEXT_PIECE;
}

void olt_draw_block(const Olt_Block *block, int wx, int wy, int color)
{
  //if (wx < 0 || wx >= 10 || wy < 0 || wy >= 20) {
  //  return;
  //}
  if (!block->is_not_null) {
    return;
  }
  int tx = TEX_BLOCK_X0 + TEX_BLOCK_DX * block->type;
  int ty = TEX_BLOCK_Y0 + TEX_BLOCK_DY * block->rotation;
  GLfloat x = -1.0 + BLOCK_W + wx * BLOCK_W;
  GLfloat y = 1.0 - BLOCK_H - wy * BLOCK_H;
  Olt_Vec3gf c = olt_unpack_color(color);
  glColor3f(c.x, c.y, c.z);
  olt_trect(x, y, BLOCK_W, BLOCK_H, tx, ty, TEX_BLOCK_W, TEX_BLOCK_H, TR_AS_IS);
}

void olt_trect(GLfloat x, GLfloat y, GLfloat w, GLfloat h, int tx, int ty, int tw, int th, int flags)
{
  GLfloat tleft = (tx) / (GLfloat)TEX_W;
  GLfloat tright = (tx + tw) / (GLfloat)TEX_W;
  GLfloat tbottom = (ty + th) / (GLfloat)TEX_H;
  GLfloat ttop = (ty) / (GLfloat)TEX_H;
  GLfloat temp = 0.0;
  if (flags & TR_FLIP_X) {
    temp = tleft;
    tleft = tright;
    tright = temp;
  }
  glBegin(GL_QUADS);
  glTexCoord2f(tleft, tbottom);
  glVertex2f(x, y);
  glTexCoord2f(tright, tbottom);
  glVertex2f(x + w, y);
  glTexCoord2f(tright, ttop);
  glVertex2f(x + w, y + h);
  glTexCoord2f(tleft, ttop);
  glVertex2f(x, y + h);
  glEnd();
}

Olt_Vec3gf olt_unpack_color(int color)
{
  const Olt_Vec3gf color_table[8] = {
    { .x = 1.0, .y = 0.5, .z = 0.5 },
    { .x = 0.5, .y = 1.0, .z = 0.5 },
    { .x = 1.0, .y = 1.0, .z = 0.0 },
    { .x = 0.5, .y = 0.5, .z = 1.0 },
    { .x = 1.0, .y = 0.0, .z = 1.0 },
    { .x = 0.0, .y = 1.0, .z = 1.0 },
    { .x = 1.0, .y = 1.0, .z = 1.0 },
    { .x = 0.5, .y = 0.5, .z = 0.5 },
  };
  if (color < 0 || color >= 8) {
    return color_table[0]; // TODO: error maybe?
  }
  return color_table[color];
}

void olt_exit(int exit_type)
{
  if (olt->glfw_inited) {
    glfwTerminate();
  }
  if (exit_type == ET_FAIL) {
    exit(EXIT_FAILURE);
  }
  else if (exit_type == ET_OK) {
    exit(EXIT_SUCCESS);
  }
  else {
    errorf("%s%i%s", "Unknown exit type: ", exit_type, "\n");
    exit(EXIT_FAILURE);
  }
}

Olt_Block olt_piece_block(int type, int rotation)
{
  Olt_Block block;
  memset(&block, 0, sizeof(block));
  block.is_not_null = true;
  block.type = type;
  block.rotation = rotation;
  return block;
}

Olt_Block *olt_block_at(int x, int y, Olt_Block *a, int w, int h)
{
  static Olt_Block dummy;
  memset(&dummy, 0, sizeof(dummy));
  dummy.is_not_null = true;
  dummy.is_outside = true;
  if (x < 0 || x >= w || y >= h) {
    return &dummy;
  }
  if (y < 0) {
    dummy.is_not_null = false;
    return &dummy;
  }
  return &a[x + y * w];
}

GLuint olt_shader_from_string(GLenum type, const char *src, const char *srcname)
{
  GLint params = 0;
  GLuint shader = 0;
  GLchar info[1024];
  GLsizei info_length = 0;
  shader = glCreateShader(type);
  glShaderSource(shader, 1, &src, NULL);
  glCompileShader(shader);
  glGetShaderiv(shader, GL_COMPILE_STATUS, &params);
  if (params != GL_TRUE) {
    glGetShaderInfoLog(shader, sizeof(info) - 1, &info_length, info);
    info[info_length] = '\0';
    errorf("%s%s%s%s", srcname, ": ", info, "\n");
    olt_exit(ET_FAIL);
  }
  return shader;
}

GLuint olt_texture_from_file(const char *filename)
{
  const int bytes_pp = 4;
  const int width = 128;
  const int height = 128;
  char pixels[width * height * bytes_pp];
  FILE *tfile = fopen(filename, "rb");
  if (tfile == NULL) {
    errorf("%s%s", filename, ": no such file or directory\n");
    olt_exit(ET_FAIL);
  }
  int size = width * height * 4;
  int readn = fread(pixels, 1, size, tfile);
  fclose(tfile);
  if (readn != size) {
    errorf("%s%s", filename, ": not enough data was read\n");
    olt_exit(ET_FAIL);
  }
  GLuint texture = 0;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
 	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  if (texture == 0) {
    errorf("%s%s%s", filename, ": error : ", "glGenTextures() failed\n");
    olt_exit(ET_FAIL);
  }
  return texture;
}

void olt_register_key(int rkey, int key, int action, bool *pressed, bool *clicked)
{
  if (!*pressed && key == rkey && action == GLFW_PRESS) {
    *pressed = true;
    *clicked = true;
  }
  if (key == rkey && action == GLFW_RELEASE) {
    *pressed = false;
  }
}

void olt_cb_key(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  (void) scancode;
  (void) mods;
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(window, GLFW_TRUE);
  }
  //
  // - Why not to use loop in here?
  // - YES! But unfortunately it's a challenge where I cannot use loops.
  //
  olt_register_key(GLFW_KEY_LEFT,  key, action, &olt->pressed_left,   &olt->clicked_left);
  olt_register_key(GLFW_KEY_RIGHT, key, action, &olt->pressed_right,  &olt->clicked_right);
  olt_register_key(GLFW_KEY_UP,    key, action, &olt->pressed_rotate, &olt->clicked_rotate);
  olt_register_key(GLFW_KEY_DOWN,  key, action, &olt->pressed_place,  &olt->clicked_place);
}

void olt_cb_window_size(GLFWwindow* window, int width, int height)
{
  (void) window;
  int x = 0;
  int y = 0;
  int s = 0;
  if (height < width) {
    s = height;
    x = (width - height) / 2;
  }
  else {
    s = width;
    y = (height - width) / 2;
  }
  glViewport(x, y, s, s);
}

////////////////////////////////////////////////////////////////
// MAIN
////////////////////////////////////////////////////////////////

int main()
{
  olt_init();
  //
  // THIS LOOP IS THE ONLY LOOP I WROTE
  // ||||
  // VVVV
  for (;;) {
    olt_loop();
  }
  return 0;
}

