CC = gcc
LD = gcc

WARNS = -Wall -Wextra

CFLAGS = -std=c99 -pedantic

LIBS = -lm -lglfw -lGLEW -lGL

OFILES = \
  olt.o

.PHONY: all clean

all: olt

olt: $(OFILES)
	$(LD) -o $@ $(OFILES) $(LIBS)

%.o: %.c
	$(CC) $(CFLAGS) $(WARNS) -c -o $@ $<

clean:
	rm -rf ./*.o

